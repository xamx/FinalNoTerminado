package cneccion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnect {
	Connection con = null;
	
	public void connect() {
		try {
			con = DriverManager.getConnection
					("jdbc:mysql://localhost:3306/mensajes"
							+ "useUnicode=true&"
							+ "useJDBCCompliantTimezoneShift=true&"
							+ "useLegacyDatetimeCode=false&"
							+ "serverTimezone=UTC","root","");
			System.out.println("Success");
			
			

		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}
	
	
	public Connection getConn() {
		return getConn();
	}


	public void setConn(Connection conn) {
		this.con = conn;
	}


	public static void main(String args[]) {
		MysqlConnect con = new MysqlConnect();
		con.connect();
	}
}
