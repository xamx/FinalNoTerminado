
package vista;

import controlador.ControladorServidor;

public interface IVista {
    final String ENVIAR = "ENVIAR";
    
    public void habilitarEnviar();
    public void deshabilitarEnviar();
    public void agnadirMensajeATrasiego(String mensaje);
    public void borrarTextoAEnviar();
    public void setControlador(ControladorServidor controlador);
    public void hacerVisible();
    public void inicializar();
    public String getMensajeAEnviar();
}
